Feature: Login Feature
  Scenario: Login as a authenticated user with correct username and password
    Given we are on homepage
    When we clicked on login button
    And we enter username is "admin" and password is "1234"
    Then The "Hello admin" text is showing

  Scenario: Login as a authenticated user with correct username and wrong password
    Given we are on homepage
    When we clicked on login button
    And we enter username is "admin" and password is "123"
    Then The "UserName or password is not correct" text is showing when cannot login

  Scenario: Login as a authenticated user and logout
    Given we are on homepage
    When we clicked on login button
    And we enter username is "admin" and password is "1234"
    Then The "Hello admin" text is showing
    When we clicked on logout button
    Then The login button is show
