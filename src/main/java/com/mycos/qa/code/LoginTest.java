package com.mycos.qa.code;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class LoginTest {
    public static WebDriver driver;
    @Given("^we are on homepage$")
    public void we_are_on_homepage() throws Throwable {   
    	
    	String path = System.getProperty("user.dir");
    	System.setProperty("webdriver.chrome.driver", path + "/chromeDriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://mycossite.azurewebsites.net/");
    }
    
    @When("^we clicked on login button$")
    public void we_click_on_login_button() throws Throwable {
        driver.findElement(By.linkText("Login")).click();
    }
    
    @When("^we enter username is \"([^\"]*)\" and password is \"([^\\\"]*)\"$")
    public void user_enters_username_and_Password(String username, String password) throws Throwable {
    	driver.findElement(By.id("Username")).sendKeys(username);
    	Thread.sleep(3000);
        driver.findElement(By.id("Password")).sendKeys(password);
        Thread.sleep(3000);
        driver.findElement(By.xpath("html/body/div[1]/main/div/div/form/div[4]/input")).click();
        Thread.sleep(3000);
    }
    
    @Then("^The \"([^\"]*)\" text is showing$")
    public void the_text_is_showing(String text) throws Throwable {  
    	WebElement element = driver.findElement(By.xpath("html/body/header/nav/div/div/p[1]/span"));
    	Assert.assertEquals(element.getText(), text);
    }
    
    @Then("^The \"([^\"]*)\" text is showing when cannot login$")
    public void the_text_is_showing_when_cannot_login(String text) throws Throwable { 
    	WebElement element = driver.findElement(By.xpath("/html/body/div[1]/main/div/div/form/div[3]/label"));
    	Assert.assertEquals(element.getText(), text);
    }
    
    @When("^we clicked on logout button$")
    public void we_clicked_on_logout_button() throws Throwable {
        driver.findElement(By.xpath("/html/body/header/nav/div/div/div/form/button")).click();
        Thread.sleep(3000);
    }
    
    @Then("^The login button is show$")
    public void the_login_button_is_show() throws Throwable { 
    	WebElement loginButton = driver.findElement(By.linkText("Login"));
    	Assert.assertTrue(loginButton.isDisplayed());;
    }
}
